
m flask import Flask, request
import json
import psycopg2
from datetime import date, timedelta, datetime

class DataBase:
  def __init__(self):
    self.host = "userdateofbirth.cvapoewgdsce.us-east-1.rds.amazonaws.com"
    self.dbname = "userDB"
    self.user = "ernest"
    self.password = "12345678"
    self.conn = None
    self.cur = None

  def db_connect(self):
    self.conn = psycopg2.connect(host=self.host, dbname=self.dbname, user=self.user, password=self.password)
  
  def db_close(self):
    self.cur.close()
    self.conn.close()
    self.conn = None
    self.cur = None
  
  def db_create(self):
    self.db_connect()
    self.cur = self.conn.cursor()
    self.cur.execute("CREATE TABLE users ( name varchar(20) PRIMARY KEY, dateOfBirth date);")
    self.conn.commit()
    self.db_close()

  def db_insert(self, name, date):
    self.db_connect()
    self.cur = self.conn.cursor()
    self.cur.execute("INSERT INTO users (name, dateOfBirth) VALUES (%s,%s) ON CONFLICT (name) DO UPDATE SET dateOfBirth = EXCLUDED.dateOfBirth;", (name, date))
    self.conn.commit()
    self.db_close()

  def db_select(self, name):
    self.db_connect()
    self.cur = self.conn.cursor()
    self.cur.execute("SELECT dateOfBirth FROM users WHERE name LIKE %s;", (name,))
    db_out = self.cur.fetchone()
    print(db_out)
    self.db_close()
    return db_out

  def db_destroy(self):
    self.db_connect()
    self.cur = self.conn.cursor()
    self.cur.execute("DROP TABLE users;")
    self.conn.commit()
    self.db_close() 

app = Flask('app')
db = DataBase()

# db.db_destroy()
# db.db_create()

def set_data(name, date_of_birth_str):
  date_of_birth = datetime.strptime(date_of_birth_str, "%Y-%m-%d").date()
  db.db_insert(name, date_of_birth)
  return True

def get_data(name):
  data = db.db_select(name)
  if data is not None:
    return data[0]
  return False

def calculate_birthday(date_of_birth):
  today = date.today()
  td = timedelta(hours=24)
  if today > date_of_birth.replace(year = today.year):
    next_birthday =  date_of_birth.replace(year = today.year + 1)
  else:
    next_birthday =  date_of_birth.replace(year = today.year)
  return (next_birthday - today) // td

@app.route('/name/<name>', methods=['PUT'])
def put(name):
  date_of_birth_str = request.json['dateOfBirth']
  set_data(name, date_of_birth_str)
  return app.response_class(status=204)

@app.route('/name/<name>', methods=['GET'])
def get(name):
  date_of_birth = get_data(name)
  if date_of_birth:
    days_to_birthday = calculate_birthday(date_of_birth)
    if days_to_birthday > 0:
      message = "Hello, {0}! Your birthday is in {1} days".format(name, days_to_birthday)
    else:
      message = "Hello, {0}! Happy Birthday".format(name)
  else:
    message = "There is no name {0} in database".format(name)
  data = {"message" : message}
  response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
  return response

app.run(host='0.0.0.0', port=8080)
